import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {Plugins} from '@capacitor/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';







@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})




export class Tab3Page {
 contact: FormGroup;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    const {App} = Plugins;
    App.addListener('backButton', () => {
      this.router.navigateByUrl('/tabs/tab1');
    });

    this.contact = this.formBuilder.group({
      name: ['', Validators.required],
      surnames: ['', Validators.required],
      comentary: ['']
    });
  }

  sendMailForContact(){
    console.log("Nombre "+this.contact.get('name').value+" Apellidos: "+this.contact.get('surnames').value+ " Comentario: "+this.contact.get('comentary').value);
  }

  
}
