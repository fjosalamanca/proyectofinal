import { Component } from '@angular/core';
import {LoadingController} from '@ionic/angular';
import {Router} from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import {NotasservicioService} from '../servicios/notasservicio.service';

// Hacemos los imports necesarios para poder escuchar los eventos de la app
import {Plugins} from '@capacitor/core';
import { fromEventPattern } from 'rxjs';




@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})

export class Tab2Page {

  options = [
    {
      value: true,
      name: 'Si'
      
    },
    {
      value: false,
      name: 'No'
      
    }
  ];

  todo: FormGroup; // Instancia del FormGroup de nueva.page.html
  myloading: any; // Se usará para mostrar un cargando mientras se realiza la operación.

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public loadingController: LoadingController,
    private notasService: NotasservicioService
    
  ) {
    // Creamos la relación entre el formulario de tab2.page.html y todo; además asociamos los validadores y valores iniciales
    this.todo = this.formBuilder.group({
      title: ['', Validators.required],
      description: [''],
      option: new FormControl(this.options[0].value)
    });
    const {App} = Plugins;
    App.addListener('backButton', () => {
      this.router.navigateByUrl('/tabs/tab1');
    });
  }
  


  /*
  Se ejecuta al submit del formulario. Crea un objeto proveniente del formulario (sería igual que this.todo.value) y
  llama a la función agregaNota del servicio. Gestiona la Promise para sincronizar la interfaz.
  */
 logForm() {
   let data = {
    title: this.todo.get('title').value,
    description: this.todo.get('description').value,
    option: JSON.parse(this.todo.get('option').value) 
   };
   console.log(this.todo.get('option').value);
   /*Mostramos el cargando..*/
   //this.myloading = this.presentLoading();
   let mutationSubscription = this.notasService.createNote(data.title, data.description, data.option).subscribe(
    ({data}) => {
      console.log('nota subida', data);
      console.log(this.todo);
      this.todo.setValue({
        title: '',
        description: '',
        option: this.options[0].value
      });
    
      // Cerramos el cargando
      //this.loadingController.dismiss();
      this.router.navigateByUrl('/tabs/tab1');
    }, (error) => {
      console.log ("No se ha podido guardar la nueva nota", error);
      //this.loadingController.dismiss();
      //Probar a mostrar el error con un toast
    }
  );
  
 }

 async presentLoading() {
   this.myloading = await this.loadingController.create({
    message: 'Guardando'
   });

   return await this.myloading.present();
 }
}
