import { Component, OnInit } from '@angular/core';
import {ModalController, LoadingController, NavParams} from '@ionic/angular';
import {FormBuilder, Validators, FormGroup, FormControl} from '@angular/forms';
import {NotasservicioService} from 'src/app/servicios/notasservicio.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})

export class ModalComponent implements OnInit {

  options = [
    {
      value: true,
      name: 'Si'
      
    },
    {
      value: false,
      name: 'No'
      
    }
  ];

  private todo: FormGroup;
  userProfileCollection: any;
  myloading: any;
  private subscription: Subscription;
  private getNoteSubscription: Subscription;
  constructor(
    private formBuilder: FormBuilder,
    public loadingController: LoadingController,
    private modalCtrl: ModalController,
    private props: NavParams,
    private notasService: NotasservicioService,
    //El objeto del servicio todoservicioService hay que eliminarlo
  ) {
    this.todo = this.formBuilder.group({
      title: ['', Validators.required],
      description: [''],
      option: new FormControl('')
    });
   }

  ngOnInit() {

    //this.myloading = this.presentLoading();

    /*Atención, así leemos las variables que se inyectan como parámetros, Ver la llamada a modalController */

    /*this.todoS.leeNota(this.props.data['id'])
    .subscribe((querySnapshot) => {
      this.todo.setValue({
        title: querySnapshot.data().title,
        description: querySnapshot.data().description
      });
      this.loadingController.dismiss();
    });*/
    this.getNoteSubscription = this.notasService.getNote(this.props.data['id'])
    .subscribe(({data, loading, errors})=>{
      this.todo.setValue({
        title: data.notas[0].title,
        description: data.notas[0].description,
        option: data.notas[0].important
      });
      console.log(data);
      //this.loadingController.dismiss();
    });

  }

  logForm() {
    //this.myloading = this.presentLoading();
    this.actualiza();
  }

  actualiza() {/*
    this.todoS.actualizaNota(this.props.data['id'], this.todo.value)
    .then(() => {
      this.loadingController.dismiss();
      this.closeModal();
    })
    .catch((err) => {
      console.log(err);
    });*/
    this.getNoteSubscription.unsubscribe();
    console.log(this.todo.value['title']);
     this.subscription = this.notasService.updateNote(this.props.data['id'], this.todo.value['title'], this.todo.value['description'], JSON.parse(this.todo.get('option').value))
    .subscribe(() => {
      //this.loadingController.dismiss();
      this.closeModal();
    });
  }

  closeModal() {
    if(this.subscription){
      this.subscription.unsubscribe();
    }
    this.modalCtrl.dismiss();
  }

  async presentLoading() {
    this.myloading = await this.loadingController.create({
      message: 'Guardando'
    });

    await this.myloading.present();
  }

}
