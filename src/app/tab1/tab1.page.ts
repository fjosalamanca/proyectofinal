import { Component, ViewChild, OnInit } from '@angular/core';
import {LoadingController, AlertController, ModalController} from '@ionic/angular';
import {Router} from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { ModalComponent } from '../modal/modal.component';
import {LoadingServiceService} from '../servicios/loading-service.service';
import {NotasservicioService} from '../servicios/notasservicio.service'
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  options = [
    {
      value: false,
      name: 'Mostrar Todas'
      
    },
    {
      value: true,
      name: 'Mostrar Importantes'
      
    }
  ];

  importance: FormGroup;

  notas: any[];
  error: any;
  loading: boolean = true;
  private querySubscription: Subscription;
  private querytrueSubscription: Subscription;


  @ViewChild('dynamicList', {static: false}) dynamicList: { closeSlidingItems: () => any; };
  listado = [];
  listadoPanel = [];
  constructor(
    public loadingController: LoadingController,
    private router: Router,
    public alertController: AlertController,
    public modalCtrl: ModalController,
    public miLoading: LoadingServiceService,
    private notasService: NotasservicioService,
    public datepipe: DatePipe,
    private formBuilder: FormBuilder,
    
  ) {
    this.importance = this.formBuilder.group({
      selectImportance: new FormControl(this.options[0].value)
    });
  }

  onOptionsSelected(value){
    if(JSON.parse(value) === true){
      console.log(this.querySubscription);
      this.querySubscription.unsubscribe();
      console.log(this.querySubscription);
      this.querytrueSubscription = this.notasService.getNotesImportant(JSON.parse(value))
      .valueChanges
      .subscribe(({data, loading, errors})=>{
        console.log(data);
        this.listado = [];
        this.notas = [];
        this.loading = loading;
        this.notas = data.notas;
        console.log(data.notas);
        this.error = errors;
        this.notas.forEach(nota => {
          if (nota.important === true) {
            this.listado.push({id: nota.id, title: nota.title, description: nota.description,important: nota.important, created_at: this.dateformat(nota.created_at), updated_at: this.dateformat(nota.updated_at)});
          }
        });
        this.listadoPanel = this.listado;
        console.log(this.listado);
        //this.miLoading.dismiss();

      });
    }else {
      this.querytrueSubscription.unsubscribe();
      this.cargaNotasDesdeGraphQl();
    }
    console.log(JSON.parse(value));
    
    
  }

  // Analizar el ciclo de vida de los componentes: justo cuando se hace activa
  ngOnInit() {
    console.log("Ejecutando OnInit");
    this.cargaNotasDesdeGraphQl();
  }
  ionViewDidLeave() {
    
    //this.querySubscription.unsubscribe();
  }
  doRefresh(refresher) {
    // Aquí el código que se ejecutará al hacer el refresh
    refresher.target.complete();
  
  }
  async delete() {
    await this.dynamicList.closeSlidingItems();
  }

  /*async presentLoading(msg) {
    let myloading = await this.loadingController.create({
      message: msg
    });
    return await myloading.present();
  }*/

  


  irNueva()  {
    console.log('Ir a Nueva');
    this.router.navigateByUrl('/tabs/tab2');
  }

  getFilteredItem(searhcbar) {
    var item = searhcbar.target.value;
    // Filtra tanto en la descripción como en el título. Se podría mejorar con RegExp
    this.listadoPanel = this.listado.filter((data) => {
      return (data.description.toLowerCase().indexOf(item.trim().toLowerCase()) > -1 ||
      data.title.toLowerCase().indexOf(item.trim().toLowerCase()) > -1 );
    });
  }

  // Se ejecuta al pulsar cancelar del buscador, devuelve listadoPanel a su valor Original

  onCancel() {
    return this.listado;
  }

  // Cuando se borra hace lo mismo que al cancelar

  cleared() {
    return this.listado;
  }
  //  Es ejecutada al pulsar el botón de borrado o deslizar hacial a izquierda totalmente
  borraNota(id) {
    // Lanza un confirm: elemento asíncrono
    this.presentAlertConfirm(id, '¿Borrar Nota?')
    .then((res) => {
      console.log('Confirma el borrado mostrado adecuadamente');
    })
    .catch((err) =>{
      console.log(err);
    });
  }

  // Devuelve una promise cuando todo ha terminado

  async presentAlertConfirm(id, msg) {
    // No pasa de esta línea hasta que es creado
    // Es lo mismo que poner this.alertController.create(...).then(alert => alert.present())

    let alert = await this.alertController.create({
      header: 'Alerta',
      message: '<strong>' + msg + '</strong>',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirmar Cancelar: blah');
          }
        },
        {
          text: 'Ok',
          role: 'ok',
          cssClass: 'danger',
          handler: () => {
            console.log('Confirm Okay');
            this.borraNotaConfirmado(id);
          }
        }
      ]
    });

    // No se ejecuta hasta que el anterior await se ha completado.
    alert.present();
  }

  borraNotaConfirmado(id) {
    this.notasService.deleteNote(id).subscribe(()=>{
      this.miLoading.dismiss();
    });
    //this.miLoading.presentLoading ('Borrando');
    /*this.todoS.borraNota(id).then(() => {
      this.miLoading.dismiss();
      this.cargaNotasDesdeGraphQl();
    })
    .catch((err) => {
      console.log(err);
    });*/
  }

  

   cargaNotasDesdeGraphQl(){
    //this.miLoading.presentLoading("Cargando");
    //this.listado = [];
    //this.notas = [];
    //this.delete();
    
    console.log("Francis");
    this.querySubscription = this.notasService.getNotes()
    .valueChanges
      .subscribe(({data, loading, errors}) => {
        this.listado = [];
        this.notas = [];
        //this.delete();
        this.loading = loading;
        this.notas = data.notas;
        console.log(data.notas);
        this.error = errors;
        this.notas.forEach(nota => {
          this.listado.push({id: nota.id, title: nota.title, description: nota.description,important: nota.important, created_at: this.dateformat(nota.created_at), updated_at: this.dateformat(nota.updated_at)});
        });
        this.listadoPanel = this.listado;
        console.log(this.listado);
        //this.miLoading.dismiss();
      });

      
  }
  // Transformamos la fecha de formato unix a un formato entendible multiplicamos por 1000 para obtener los milisegundos. 
  dateformat(date): string {
    return this.datepipe.transform(date*1000, 'd MMM H:mm')
  }

  editaNota(id) {
    this.presentModal(id);
  }

  async presentModal(id) {
    const modal = await this.modalCtrl.create({
      component: ModalComponent,
      componentProps: { id: id }
    });

    modal.onDidDismiss()
    .then(() => {
      //this.cargaNotasDesdeGraphQl();
      this.modalCtrl.dismiss();
    });
    return await modal.present();
  }

  mostrarEvento($event) {
    console.log($event);
  }
}
