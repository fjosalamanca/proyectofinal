import { Injectable } from '@angular/core';

//Importamos los Módulos de Apollo
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';

const nuevaNota =  gql`
mutation addNote ($title: String!, $description: String!, $important: Boolean!) {
        addNote (title: $title, description: $description, important: $important) {
        title
        description
        }
}
`;

const getNotas = gql`
   query notas{
     notas {
        id
        title
        description
        created_at
        updated_at
        important
     }
    }
`;

const getNote = gql`
    query notas ($id: Int!) {
      notas (id: $id) {
        id
        title
        description
        important
      }
    }
`;

const getNotesImportant = gql`
query notas ($important: Boolean!) {
  notas (important: $important) {
    id
    title
    description
    created_at
    updated_at
    important
  }
}
`;

const mutationDeleteNote = gql`
  mutation deleteNote ($id: Int!) {
    deleteNote (id: $id) {
      id
      title
      description
    }
  }
`;

const mutationUpdateNote = gql`
mutation updateNote ($id: Int!, $title: String!, $description: String!, $important: Boolean!) {
  updateNote (id: $id, title: $title, description: $description, important: $important) {
    id
    title
    description
  }
}
`;


@Injectable({
  providedIn: 'root'
})


export class NotasservicioService {
 
  constructor(
    private apollo: Apollo,
  ) { }


  getNotes () {
    return this.apollo
      .watchQuery<any>({
        query: getNotas,
      });
  }

  getNote (id) {
    return this.apollo
    .query<any>({
      query: getNote,
      variables: {
        id: id,
      },
    });
  }

  getNotesImportant (important) {
     
    return this.apollo
    .watchQuery<any>({
      query: getNotesImportant,
      variables: {
        important: important,
      }
    });
  }
  
  createNote (title: string, description: string, important: boolean) {
   return this.apollo.mutate ({
      mutation: nuevaNota,
      variables: {
        title: title,
        description: description,
        important: important
      },
      refetchQueries: [{
        query: getNotas,
        variables: {
          title: title,
          description: description,
          important: important
        }
      },
      {
        query: getNotesImportant,
        variables: {
          title: title,
          description: description,
          important: important
        }
      }],
    });
    /*return this.apollo.mutate({
      mutation: nuevaNota,
      variables: {
        title: title,
        description: description,
      },
      update: (proxy, {data: {addNote}}) => {
        const data = proxy.readQuery({query: getNotas});
        data..push(addNote);
        proxy.writeQuery({query: getNotas, data});
      }
    });*/
  }

  updateNote (id: number, title: string, description: string, important: Boolean){
    return this.apollo.mutate ({
      mutation: mutationUpdateNote,
      variables: {
        id: id,
        title: title,
        description: description,
        important: important
      },
      refetchQueries: [{
        query: getNotesImportant,
        variables: {
          important: important
        }
      }]
    });
  }

  deleteNote (id: number, important: boolean = true){
    return this.apollo.mutate ({
      mutation: mutationDeleteNote,
      variables: {
        id: id,
      },
      refetchQueries: [{
        query: getNotas,
      },
      {
        query: getNotesImportant,
        variables: {
          important: important
        }
      }]
    });
  } 
}
