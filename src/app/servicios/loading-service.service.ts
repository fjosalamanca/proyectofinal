import { Injectable } from '@angular/core';
import {LoadingController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingServiceService {
  isLoading = false;
  constructor(
      public loadingController: LoadingController
  ) {
      
   }

   async presentLoading(msg) {
    this.isLoading = true;
    return await this.loadingController.create({message: msg}).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss();
        }
      });
    });
  }
  async dismiss() {
    if (this.isLoading) {
      this.isLoading = false;
      return await this.loadingController.dismiss();
      this.loadingController.getTop
    }
    return null;
  }
}
