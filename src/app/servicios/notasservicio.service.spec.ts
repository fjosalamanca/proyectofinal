import { TestBed } from '@angular/core/testing';

import { NotasservicioService } from './notasservicio.service';

describe('NotasservicioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotasservicioService = TestBed.get(NotasservicioService);
    expect(service).toBeTruthy();
  });
});
